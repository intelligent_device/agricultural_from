const resolve = require('path').resolve
const path = require('path')
// const CopyWebpackPlugin = require('copy-webpack-plugin')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const url = require('url')
const publicPath = ''

module.exports = (options = {}) => ({
  entry: {
    index: ["babel-polyfill", "./src/main.js"],
    // vendor: './src/vendor',
    // index: './src/main.js',
    // externals: {
    //   'AMap': 'AMap'
    // },    
  },
  output: {
    path: resolve(__dirname, 'dist/assets'),
    // filename:'dist/static/js/[name].js'
    filename: options.dev ? 'js/[name].js' : 'js/[name].js?[chunkhash]',
    chunkFilename: '[id].js?[chunkhash]',
    publicPath: options.dev ? '/assets/' : publicPath
  },
  module: {
    rules: [{
        test: /\.vue$/,
        use: ['vue-loader']
      },
      {
        test: /\.sass$/,
        loaders: ['style', 'css', 'sass']
      }, 
      {
        test: /\.less$/,
        loader: "style-loader!css-loader!less-loader",
      },
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
             presets: ['es2015']
          }
        }, include: [resolve('src'), resolve('test'),resolve('node_modules/_swiper@4.5.1@swiper/dist'),resolve('node_modules/dom7/dist')]
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader', 'postcss-loader']
      },
      {
        test: /\.(png|jpg|jpeg|gif|eot|ttf|woff|woff2|svg|svgz)(\?.+)?$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
            name:'img/[name]-[hash:6].[ext]'
          }
        }]
      }
    ]
  },
  plugins: [
// s
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor', 'manifest']
    }),
    new HtmlWebpackPlugin({
      template: 'src/index.html'
    })
  ],
  resolve: {
    alias: {
      '~': resolve(__dirname, 'src')
    },
    extensions: ['.js', '.vue', '.json', '.css']
  },
  devServer: {
    host: '127.0.0.1',
    port: 8070,
    proxy: {
      '/res/': {
        // target: 'http://192.168.0.128:9020/hzpris-api',
        target: 'http://123.56.78.140:88/res',
        changeOrigin: true,
        pathRewrite: {
          '^/res': '/'
        }
      }
    },
    historyApiFallback: {
      index: url.parse(options.dev ? '/assets/' : publicPath).pathname
    }
  },
  devtool: options.dev ? '#eval-source-map' : '#source-map'
})

