import Vue from 'vue'; //首先引入vue
import Vuex from 'vuex'; //引入vuex
import { getDetail, setDetail  } from './auth'
Vue.use(Vuex) 

export default new Vuex.Store({
    state: { 
        detailArr: getDetail('dname'),
        rule_detailArr: getDetail('rule_dname'),
        news_detailArr: getDetail('news_dname'),
        home_detailArr: getDetail('home_dname'),
        // state 类似 data
        //这里面写入数据
    },
    getters:{ 
        // getters 类似 computed 
        // 在这里面写个方法
    },
    mutations:{ 
        detailNameChage(state,msg){
            state.detailName = msg;
            setDetail('dname',msg)
        },
        rule_detailNameChage(state,msg){
            state.rule_detailArr = msg;
            setDetail('rule_dname',msg)
        }, 
        news_detailNameChage(state,msg){
            state.news_detailArr = msg;
            setDetail('news_dname',msg)
        },
        home_detailNameChage(state,msg){
            state.home_detailArr = msg;
            setDetail('home_dname',msg)
        },                            
        // mutations 类似methods
        // 写方法对数据做出更改(同步操作)
    },
    actions:{
        // actions 类似methods
        // 写方法对数据做出更改(异步操作)
    }
})

