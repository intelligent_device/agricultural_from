export function  setDetail(name, data) { //localStorage 获取数组对象的方法
  return localStorage.setItem(name, JSON.stringify(data))
}

export function  getDetail(name) { //localStorage 获取数组对象的方法
  return JSON.parse(window.localStorage.getItem(name));
}

