import Vue from 'vue'
import Router from 'vue-router'
import index from './views/index.vue'
import projectAnnouncement from './views/projectAnnouncement'
import policyRule from './views/policyRule'
import dealRule from './views/dealRule'
import aboutUs from './views/aboutUs'
import dealDetails from './views/dealDetails'
import policyDetails from './views/policyDetails'
import search from './views/search'

import transfer_detail from './views/project/transfer/detail.vue'
import formal_detail from './views/project/formal/detail.vue'
import realtor_detail from './views/project/realtor/detail.vue'
import increase_detail from './views/project/increase/detail.vue'
import assettransfer_detail from './views/project/assettransfer/detail.vue'

import clinch_detail from './views/notice/clinch.vue'
import zz_detail from './views/notice/zz.vue'

Vue.use(Router);

const router=new Router({
  mode:'history',
  routes: [
    {
      path: '/',
      name: 'index',
      component: index,
      meta: { title: '首页' }
    },                 
    {
      path: '/projectAnnouncement',
      name: 'projectAnnouncement',
      component: projectAnnouncement,
      meta: { title: '项目公告' }
    },                 
    {
      path: '/policyRule',
      name: 'policyRule',
      component: policyRule,
      meta: { title: '政策法规' }
    },                 
    {
      path: '/dealRule',
      name: 'dealRule',
      component: dealRule,
      meta: { title: '交易规则' }
    },                 
    {
      path: '/aboutUs',
      name: 'aboutUs',
      component: aboutUs,
      meta: { title: '关于我们' }
    },                 
    {
      path: '/dealDetails',
      name: 'dealDetails',
      component: dealDetails,
      meta: { title: '交易规则详情' }
    },                 
    {
      path: '/policyDetails',
      name: 'policyDetails',
      component: policyDetails,
      meta: { title: '交易规则详情' }
    },        
    {
      path: '/search',
      name: 'search',
      component: search ,
      meta: { title: '搜索' }
    },
    
    {
      path: '/transfer_detail',
      name: 'transferDetail',
      component: transfer_detail,
      meta: { title: '预披露项目详情' }
    },{
      path: '/formal_detail',
      name: 'formalDetail',
      component: formal_detail,
      meta: { title: '正式披露项目详情' }
    },{
      path: '/realtor_detail',
      name: 'realtorDetail',
      component: realtor_detail,
      meta: { title: '房屋租赁项目详情' }
    },{
      path: '/increase_detail',
      name: 'increaseDetail',
      component: increase_detail,
      meta: { title: '企业增资项目详情' }
    },{
      path: '/assettransfer_detail',
      name: 'assettransferDetail',
      component: assettransfer_detail,
      meta: { title: '资产转让项目详情' }
    },
    
    {
      path: '/clinch_detail',
      name: 'clinchDetail',
      component: clinch_detail,
      meta: { title: '成交公告详情' }
    },{
      path: '/zz_detail',
      name: 'zzDetail',
      component: zz_detail,
      meta: { title: '中止终结公告详情' }
    },
  ]
});
export default router
router.beforeEach((to, from, next) => {
  // console.log('全局前置守卫：beforeEach -- next需要调用');
  // console.log(to)
  // console.log(from)
  next();
});