// import axios from 'axios';
// import qs from 'qs';
// import 'es6-promise';
// let base_url="/res";
// // 暴露全局变量作用
// axios.defaults.withCredentials = true;//允许cookei跨域
// axios.defaults.timeout = 50000;//请求超时时间
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
// axios.defaults.baseURL = base_url;
 
// export function get(url, params) {
//   return new Promise((resolve, reject) => {
//     axios.get(url, {
//       params: params
//     }).then(res => {
//       resolve(res.data)
//     }).catch(err => {
//       reject(err)
//     })
//   })
// }
 
// export function post(url, data) {
//   return new Promise((resolve, reject) => {
//     axios.post(url, qs.stringify(data), {
//         headers: {
//           'Content-Type': 'application/x-www-form-urlencoded',
//         }
//       }
//     ).then(res => {
//       resolve(res.data)
//     }).catch(err => {
//       reject(err)
//     })
//   })
// }


import axios from 'axios'
import qs from 'qs'
import 'es6-promise'

axios.defaults.baseURL = '/res';
// axios.defaults.baseURL = 'http://nccq.hzaee.com.cn/api';

export function get(url, params) {
  return new Promise((resolve, reject) => {
    axios.get(url, {
      params: params
    }).then(res => {
      resolve(res.data)
    }).catch(err => {
      reject(err)
    })
  })
}

export function post(url, data) {
  return new Promise((resolve, reject) => {
    axios.post(url, qs.stringify(data), {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          }
        }
    ).then(res => {
      resolve(res.data)
    }).catch(err => {
      reject(err)
    })
  })
}