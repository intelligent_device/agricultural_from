import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import router from './router'
import '~/assets/index.css' // global css
import '~/components/static/css/header.css'
import '~/components/static/css/footer.css'
import axios from 'axios'
import store from './store/store'
import {post,get} from './request/http'
import Router from 'vue-router'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'
import AMap from 'vue-amap'
// import 'viewerjs/dist/viewer.css';
// import Viewer from 'v-viewer';
// Vue.use(Viewer);
Vue.use(AMap)
Vue.prototype.post=post;
Vue.prototype.get=get;
Vue.use(VueAwesomeSwiper, /* { default global options } */)
const routerPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error=> error)
}
// import '~/assets/element-variables.scss'
Vue.use(ElementUI)
　AMap.initAMapApiLoader({
　　　key: 'a8c32015b78bac72799e1374c29eda33',
　　　plugin: ['AMap.Autocomplete', 'AMap.PlaceSearch', 'AMap.Scale', 'AMap.OverView', 'AMap.ToolBar', 'AMap.MapType', 'AMap.PolyEditor', 'AMap.CircleEditor']

});
Vue.prototype.axios=axios;
new Vue({
  store,
	router,
  el: '#app',
  render: h => h(App)
})
